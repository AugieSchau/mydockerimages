-- This creates a database with intial data

create database phone;
use phone;
create table numbers (
    name varchar(50),
    number varchar(30)
);

-- populate some data
INSERT into numbers VALUES ('Steve Shilling', '555-1234');
INSERT into numbers VALUES ('Augie Schau', '555-4321');
INSERT into numbers VALUES ('Will Provost', '555-9999');
