CREATE DATABASE sample;

USE sample;

CREATE TABLE trainers (
trainerId INT AUTO_INCREMENT,
fname VARCHAR(50) NOT NULL,
lname VARCHAR(60) NOT NULL,
start_date DATE,
PRIMARY KEY (trainerId)
);
